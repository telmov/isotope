# Changelog

## v2.0.2

+ Add gutter option for fitRows [#580](https://github.com/metafizzy/isotope/issues/580)
+ Fix `updateSortData` with empty Array or jQuery object

## v2.0.1

+ added `getFilteredItemElements` method [#768](https://github.com/metafizzy/isotope/issues/768)
+ added `shuffle` method
+ Fix display on `destroy` [#741](https://github.com/metafizzy/isotope/issues/741)